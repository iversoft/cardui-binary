// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "CardUI",
    platforms: [
        .macOS(.v10_14), .iOS(.v13), .tvOS(.v13)
    ],
    products: [
        .library(
            name: "CardUI",
            targets: ["CardUI"])
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "CardUI"
        ),
        .binaryTarget(
            name: "CardUI",
            url: "https://bitbucket.org/iversoft/cardui-binary/raw/0aa4832b63dcd54a4853abd13e67c1d16fbbdb0c/CardUI_iOS.xcframework.zip",
            checksum: "4f479c0ddade3544a9229f92030688161d93080aaba717ec47e332a22c3f2250"
        ),
    ]
)